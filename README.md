# React - Movie database application

## Installation

1. Clone repository

```bash
$ git clone https://bitbucket.org/tomislav-arambasic/react-movies-api
```

2. cd into project directory

```bash
$ cd react-movies-api
```

3. Install all npm packages

```bash
$ npm install
```

4. Run npm development script

```bash
$ npm start
```

5. Browse to http://localhost:3000
