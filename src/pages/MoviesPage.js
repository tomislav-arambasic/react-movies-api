import React from "react";
import { withRouter } from "react-router-dom";

import axios from "axios";

import Card from "../components/Card/Card";
import Button from "../components/Button/Button";
import RouletteModal from "../components/RouletteModal/RouletteModal";

class MoviesPage extends React.Component {
  state = {
    APImoviesPage: 1,
    moviesArray: [],
    shownMoviesArray: [],
    showModal: false
  };

  APIkey = "046458980fd6ac934f3818ed10ece3b7";
  genres = [];

  style = {
    loadButton: {
      color: "red"
    },
    container: {
      marginBottom: "150px",
      marginTop: "100px"
    }
  };

  componentDidMount() {
    this.getMovies(1);
    this.props.activateLoader(false);
  }

  getMovies = APImoviesPage => {
    this.setState({
      loadingActive: true
    });
    axios
      .get(
        `https://api.themoviedb.org/3/movie/popular?api_key=${this.APIkey}&language=en-US&page=${APImoviesPage}`
      )
      .then(res => {
        this.updateAllMoviesArray(res.data.results);
        this.updateShownMoviesArray(APImoviesPage);
      })
      .catch(() => {
        throw new Error("Something went wrong.");
      });
  };

  updateAllMoviesArray = data => {
    const currentAllMoviesArray = this.state.moviesArray;
    const updatedAllMoviesArray = currentAllMoviesArray.concat(data);
    this.setState({
      moviesArray: [...updatedAllMoviesArray]
    });
  };

  updateShownMoviesArray = APImoviesPage => {
    let currentAllMoviesArray = [...this.state.moviesArray];
    const currentShownMoviesArray = this.state.shownMoviesArray.concat([
      ...currentAllMoviesArray.splice(0, 6)
    ]);
    this.setState({
      moviesArray: [...currentAllMoviesArray],
      shownMoviesArray: [...currentShownMoviesArray],
      loadingActive: false,
      APImoviesPage: APImoviesPage
    });
  };

  loadMoreHander = () => {
    this.props.activateLoader(true);
    if (this.state.moviesArray.length < 6) {
      this.getMovies(this.state.APImoviesPage + 1);
    } else {
      this.setState({
        loadingActive: true
      });
      let currentAllMoviesArray = [...this.state.moviesArray];
      let currentShownMoviesArray = [...this.state.shownMoviesArray];

      let newShownMoviesArray = currentShownMoviesArray.concat(
        currentAllMoviesArray.splice(0, 6)
      );

      this.setState({
        moviesArray: [...currentAllMoviesArray],
        shownMoviesArray: [...newShownMoviesArray],
        loadingActive: false
      });
    }
    this.props.activateLoader(false);
  };

  openDetailsHandler = movie => {
    this.props.activateLoader(true);
    this.props.history.push({
      pathname: "/movies-API/movie?=" + movie.id,
      props: {
        guest_session_id: this.props.guest_session_id,
        image: "https://image.tmdb.org/t/p/original" + movie.backdrop_path,
        activateLoader: this.props.activateLoader
      },
      search: this.props.guest_session_id
    });
  };

  openRouletteModalHandler = () => {
    this.props.activateLoader(true);
    axios
      .get(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=${this.APIkey}&language=en-US`
      )
      .then(res => {
        this.setState({
          genres: res.data.genres,
          showModal: true
        });
        this.props.activateLoader(false);
      })
      .catch(() => {
        throw new Error("Something went wrong.");
      });
  };

  closeRouletteModalHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { showModal } = this.state;
    return (
      <div className="container" style={this.style.container}>
        <div className="row">
          {this.state.shownMoviesArray.map(movie => {
            return (
              <div
                onClick={() => this.openDetailsHandler(movie)}
                className="col-12 col-md-6 col-lg-4 mt-4 mb-4"
                key={movie.id}
              >
                <Card movie={movie}></Card>
              </div>
            );
          })}
          <Button click={this.loadMoreHander} text="Load more" />

          <Button
            click={this.openRouletteModalHandler}
            text="Roulette"
            spin="fa fa-spinner w3-spin"
          />

          <RouletteModal
            genres={this.state.genres}
            openMovieDetails={this.openDetailsHandler}
            showModal={showModal}
            close={this.closeRouletteModalHandler}
          />
        </div>
      </div>
    );
  }
}

export default withRouter(MoviesPage);
