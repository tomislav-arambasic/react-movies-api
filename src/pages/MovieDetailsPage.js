import React from "react";

import axios from "axios";
import moment from "moment";

import DescriptionOverlay from "../components/DescriptionOverlay/DescriptionOverlay";
import StarRatings from "react-star-ratings";

class MovieDetailsPage extends React.Component {
  state = {
    release_date: "",
    title: "",
    overview: "",
    vote_average: undefined,
    popularity: undefined,
    original_language: "",
    myRating: null,
    ratingColor: "orange",
    user_ratings: [],
    loadingActive: true
  };

  APIkey = "046458980fd6ac934f3818ed10ece3b7";
  companies = [];
  changeRating = this.changeRating.bind(this);

  style = {
    color: "white",
    marginTop: "100px",
    imageContainer: {
      border: "2px solid rgba(255,153,51, 0.8)",
      height: "500px",
      marginTop: "50px",
      position: "relative",
      backgroundImage: `url(${this.props.location.props.image})`,
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      overflow: "hidden"
    },
    descriptionContainer: {
      marginTop: "40px",
      marginBottom: "80px",
      position: "relative"
    },
    ratings: {
      position: "absolute",
      right: "20px"
    }
  };

  componentDidMount() {
    this.getMovieData();
    this.getUserRatings();
  }

  getMovieData = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${this.props.match.params.id}?api_key=${this.APIkey}&language=en-US`
      )
      .then(movie => {
        this.companies = movie.data.production_companies.map(o => {
          return o["name"];
        });
        this.setState({
          id: movie.data.id,
          release_date: movie.data.release_date,
          title: movie.data.title,
          overview: movie.data.overview,
          vote_average: movie.data.vote_average,
          popularity: movie.data.popularity,
          original_language: movie.data.original_language,
          loadingActive: false
        });
      })
      .catch(() => {
        throw new Error("Something went wrong.");
      });
  };

  getUserRatings = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/guest_session/${this.props.location.props.guest_session_id}/rated/movies?api_key=${this.APIkey}&language=en-US&sort_by=created_at.asc`
      )
      .then(userRatings => {
        this.setState({
          user_ratings: userRatings.data.results
        });
        this.checkIfUserRated();
      })
      .catch(() => {
        throw new Error("Something went wrong.");
      });
  };

  checkIfUserRated = () => {
    const ratedMovie = this.state.user_ratings.find(
      movie => movie.id === this.state.id
    );
    if (ratedMovie) {
      this.setState({
        myRating: ratedMovie.rating,
        ratingColor: "red"
      });
    }
    this.props.location.props.activateLoader(false);
  };

  changeRating(newRating) {
    axios.post(
      `https://api.themoviedb.org/3/movie/${this.state.id}/rating?api_key=${this.APIkey}&guest_session_id=${this.props.location.props.guest_session_id}`,
      {
        value: newRating
      }
    );

    this.setState({
      myRating: newRating,
      ratingColor: "red"
    });
  }

  render() {
    const {
      release_date,
      title,
      overview,
      vote_average,
      popularity,
      original_language
    } = this.state;

    return (
      <div className="container" style={this.style}>
        <h2>
          {title} ({moment(release_date).format("YYYY")})
        </h2>
        <div style={this.style.imageContainer}>
          <DescriptionOverlay description={overview} />
        </div>
        <div style={this.style.descriptionContainer}>
          <div className="row">
            <div className="col-12 col-md-7">
              <p>
                <b>Rating:</b> {vote_average}
              </p>
              <p>
                <b>Popularity:</b> {popularity}
              </p>
              <p>
                <b>Language:</b> {original_language.toUpperCase()}
              </p>
              <p>
                <b>Production companies:</b> {this.companies.join(", ")}
              </p>
            </div>
            <div className="col-12 col-md-5">
              <div style={this.style.ratings}>
                <StarRatings
                  rating={this.state.myRating || vote_average}
                  starRatedColor={this.state.ratingColor || "orange"}
                  changeRating={this.changeRating}
                  numberOfStars={10}
                  name="rating"
                  starDimension="25px"
                  starSpacing="2px"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MovieDetailsPage;
