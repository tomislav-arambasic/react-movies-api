import React from "react";
import axios from "axios";

class RouletteForm extends React.Component {
  constructor(props) {
    super(props);

    this.genres = this.props.genres;
    this.state = {
      genre: this.genres[0]
    };
  }

  selectGenre = selectedGenre => {
    this.setState({
      genre: selectedGenre
    });
  };

  randomizeMovie = () => {
    axios
      .get(
        "https://api.themoviedb.org/3/discover/movie?api_key=046458980fd6ac934f3818ed10ece3b7&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=" +
          this.state.genre.id
      )
      .then(res => {
        const randomMovie =
          res.data.results[Math.floor(Math.random() * res.data.results.length)];

        this.props.openMovieDetails(randomMovie);
      });
  };

  render() {
    return (
      <React.Fragment>
        {this.genres.map(genre => {
          return (
            <div className="form-check mt-20" key={genre.id}>
              <label>
                <input
                  type="radio"
                  name="randomize"
                  value={genre.id}
                  checked={this.state.genre === genre}
                  onChange={() => this.selectGenre(genre)}
                  className="form-check-input"
                />
                {genre.name}
              </label>
            </div>
          );
        })}

        <div className="form-group">
          <button
            className="btn btn-primary mt-2"
            onClick={this.randomizeMovie}
          >
            Roll
          </button>
        </div>
      </React.Fragment>
    );
  }
}

export default RouletteForm;
