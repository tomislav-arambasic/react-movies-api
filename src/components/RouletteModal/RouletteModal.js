import React from "react";
import Modal from "react-responsive-modal";
import RouletteForm from "../RouletteForm/RouletteForm";

const RouletteModal = props => {
  const style = {
    width: "300px"
  };

  return (
    <Modal open={props.showModal} onClose={props.close}>
      <h2>Movie Roulette</h2>
      <div style={style}>
        <b>Choose genre:</b>
        <RouletteForm
          genres={props.genres}
          openMovieDetails={props.openMovieDetails}
        />
      </div>
    </Modal>
  );
};

export default RouletteModal;
