import React from "react";

const CardRating = props => {
  const style = {
    margin: "auto",
    position: "absolute",
    right: "0",
    marginTop: "-20px",
    fontWeight: "bold",
    background: "radial-gradient( 5px -9px, circle, white 8%, red 26px )",
    backgroundColor: "orange",
    border: "2px solid white",
    borderRadius: "50%",
    boxShadow: "1px 1px 1px black",
    color: "white",
    font: "15px",
    height: "3rem",
    minWidth: "3rem",
    padding: "8px",
    textAlign: "center",
    zIndex: "1"
  };
  return <div style={style}>{props.rating}</div>;
};

export default CardRating;
