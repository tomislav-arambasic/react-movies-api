import React from "react";

const DescriptionOverlay = props => {
  const style = {
    position: "absolute",
    bottom: "0",
    paddingBottom: "5px",
    height: "100px",
    background: "rgba(0,0,0, 0.4)",
    color: "rgba(255,255,255, 0.8)",
    width: "100%",
    textOverflow: "clip",
    overviewContainer: {
      padding: "8px"
    }
  };
  return (
    <div style={style}>
      <div style={style.overviewContainer}>{props.description}</div>
    </div>
  );
};

export default DescriptionOverlay;
