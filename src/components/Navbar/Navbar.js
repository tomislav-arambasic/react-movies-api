import React from "react";
import { Link } from "react-router-dom";
import Radium from "radium";
import logo from "../../assets/logo1.png";

class Navbar extends React.Component {
  style = {
    height: "60px",
    backgroundColor: "rgba(255,153,51, 1)",
    margin: "0 0 50px 0",
    position: "fixed",
    top: "0",
    width: "100%",
    zIndex: "2",
    color: "white",
    borderBottom: "3px solid teal",
    ":hover": {
      cursor: "pointer"
    },
    logo: {
      marginTop: "10px"
    },
    text: {
      height: "40px",
      width: "250px",
      color: "black",
      fontSize: "40px",
      fontWeight: "bolder"
    }
  };

  render() {
    return (
      <Link to={"/"}>
        <div style={this.style}>
          <div className="container">
            <div className="row">
              <div className="col-3 col-md-1">
                <img
                  src={logo}
                  alt="logo"
                  height="40px"
                  style={this.style.logo}
                />
              </div>
              <div className="col-9 col-md-11">
                <div style={this.style.text}>MOVIE API</div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    );
  }
}

export default Radium(Navbar);
