import React from "react";

const CardImage = props => {
  const style = {
    position: "relative",
    height: "100%"
  };

  return (
    <img
      style={style}
      src={"https://image.tmdb.org/t/p/w342" + props.imageLink}
      alt="Movie poster"
      width="100%"
    />
  );
};

export default CardImage;
