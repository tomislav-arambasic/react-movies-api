import React from "react";
import Radium from "radium";

import CardImage from "../CardImage/CardImage";
import CardDescription from "../CardDescription/CardDescription";
import CardRating from "../CardRating/CardRating";

const Card = props => {
  const style = {
    boxShadow: "0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24)",
    transition: "all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1)",
    background: "rgba(255,255,255, 0.8)",
    overflow: "hidden",
    paddingBottom: "10px",
    height: "100%",
    ":hover": {
      boxShadow:
        "0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22)",
      cursor: "pointer"
    },
    descriptionContainer: {
      height: "20%"
    },
    imageContainer: {
      height: "80%"
    }
  };
  return (
    <div style={style}>
      <CardRating rating={props.movie.vote_average} />
      <div style={style.imageContainer}>
        <CardImage imageLink={props.movie.poster_path} />
      </div>
      <div style={style.descriptionContainer}>
        <CardDescription movie={props.movie} />
      </div>
    </div>
  );
};

export default Radium(Card);
