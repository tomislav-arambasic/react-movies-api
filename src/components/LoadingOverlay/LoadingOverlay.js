import LoadingOverlay from "react-loading-overlay";

const LoadingOverlay = props => {
  return (
    <LoadingOverlay active={props.active}>{props.children}</LoadingOverlay>
  );
};
export default LoadingOverlay;
