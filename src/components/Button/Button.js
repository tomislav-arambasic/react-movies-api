import React from "react";
import Radium from "radium";

const Button = props => {
  const style = {
    rouletteButtonStyle: {
      bottom: "30px",
      right: "10%",
      position: "fixed",
      width: "5rem",
      height: "5rem",
      padding: "25px 3px 15px 3px",
      border: "3px dashed white",
      borderRadius: "50%",
      backgroundColor: "teal",
      textAlign: "center",
      color: "white",
      fontWeight: "500",
      fontSize: "13px",
      ":hover": {
        boxShadow:
          "0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22)",
        cursor: "pointer",
        backgroundColor: "red"
      }
    },
    loadMoreButtonStyle: {
      margin: "0 auto",
      width: "5rem",
      padding: "10px 3px 15px 3px",
      border: "3px dashed thistle",
      borderRadius: "50%",
      backgroundColor: "seagreen",
      textAlign: "center",
      color: "white",
      fontWeight: "500",
      ":hover": {
        boxShadow:
          "0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22)",
        cursor: "pointer",
        backgroundColor: "teal"
      }
    },
    centerContainer: {
      width: "100%",
      marginTop: "20px"
    }
  };

  return (
    <div style={style.centerContainer}>
      <div
        className={props.spin}
        style={
          props.text === "Roulette"
            ? style.rouletteButtonStyle
            : style.loadMoreButtonStyle
        }
        onClick={props.click}
      >
        {props.text}
      </div>
    </div>
  );
};

export default Radium(Button);
