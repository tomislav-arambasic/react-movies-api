import React from "react";

import moment from "moment";

const CardDescription = props => {
  const style = {
    description: {
      fontWeight: "500",
      padding: "15px",
      textAlign: "center",
      borderTop: "4px solid orange",
      height: "100%"
    },
    title: {
      height: "80%"
    }
  };
  return (
    <div style={style.description}>
      <div style={style.title}>
        <h4>
          <b>
            {props.movie.original_title} (
            {moment(props.movie.release_date).format("YYYY")})
          </b>
        </h4>
      </div>
      Language: {props.movie.original_language.toUpperCase()}
    </div>
  );
};

export default CardDescription;
