import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import axios from "axios";

import MoviesPage from "./pages/MoviesPage";
import MovieDetailsPage from "./pages/MovieDetailsPage";
import Navbar from "./components/Navbar/Navbar";

import LoadingOverlay from "react-loading-overlay";

class App extends React.Component {
  APIkey = "046458980fd6ac934f3818ed10ece3b7";

  state = {
    guest_session_id: "",
    navigateHome: null,
    loading: false
  };

  componentDidMount() {
    this.getGuestSessionId();
  }

  getGuestSessionId = () => {
    axios
      .get(
        "https://api.themoviedb.org/3/authentication/guest_session/new?api_key=" +
          this.APIkey +
          "&language=en-US1"
      )
      .then(sessionId => {
        this.setState({
          guest_session_id: sessionId.data.guest_session_id
        });
      });
  };

  activateLoadingOverlay = active => {
    this.setState({
      loading: active
    });
  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <LoadingOverlay active={this.state.loading}>
            <Navbar />
            <Switch>
              <Route
                path={"/movies-API/movie?=:id"}
                activateLoader={this.activateLoadingOverlay}
                component={MovieDetailsPage}
              />
              <Route
                path="/movies-API/"
                render={() => (
                  <MoviesPage
                    guest_session_id={this.state.guest_session_id}
                    activateLoader={this.activateLoadingOverlay}
                  />
                )}
              />
              <Route
                path="/"
                render={() => (
                  <Redirect
                    to={{
                      pathname: "/movies-API/"
                    }}
                  />
                )}
              />
            </Switch>
          </LoadingOverlay>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
